import java.net.http.HttpClient;

public class Example {

    public static void main(String[] args) throws ClassNotFoundException {
        LoaderClass loaderClass = new LoaderClass();
        Class<?> exampleClass = loaderClass.findClass(Example.class.getName());
        Class<?> httpClientClass = loaderClass.findClass(HttpClient.class.getName());
        print(exampleClass.toString());
        print(httpClientClass.toString());
    }

    private static void print(String message) {
        System.out.println(message);
    }
}
