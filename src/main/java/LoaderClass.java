import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class LoaderClass extends ClassLoader {

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        String nameClass = name.replace('.', File.separatorChar) + ".class";
        File fileClass = new File(nameClass);
        if (!fileClass.exists()) {
            return findSystemClass(name);
        }
        byte[] bytesClass = loadClassFromFile(fileClass);
        return defineClass(name, bytesClass, 0, bytesClass.length);
    }

    private byte[] loadClassFromFile(File fileName) {
        byte[] result = new byte[(int) fileName.length()];
        try (FileInputStream f = new FileInputStream(fileName)) {
            f.read(result, 0, result.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
